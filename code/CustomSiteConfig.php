<?php

class CustomSiteConfig extends DataExtension {

    public static $db = array(

    );
    public static $has_one = array(

    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab('Root.Main', new GridField('TopMenuItem','Top Menu Items',TopMenuItem::get(), GridFieldConfig_RecordEditor::create()));
        $fields->addFieldsToTab('Root.Main', new GridField('BottomMenuItem','Bottom Menu Items',BottomMenuItem::get(), GridFieldConfig_RecordEditor::create()));
    }

}