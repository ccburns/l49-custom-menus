<?php
class TopMenuItem extends DataObject{

    private static $db = array(
        'Title'=>'Text',
        'DestinationURL'=>'Text',
        'OpenNewWindow'=>'Boolean'
    );

    private static $has_one = array(
        'SliderImage' => 'Image'
    );

    public function canView($member = null) {
        return Permission::check("ACCESS_DATA_OBJECT");
    }

    public function canEdit($member = null) {
        return Permission::check("ACCESS_DATA_OBJECT");
    }

    public function canDelete($member = null) {
        return Permission::check("ACCESS_DATA_OBJECT");
    }

    public function canCreate($member = null) {
        return Permission::check("ACCESS_DATA_OBJECT");
    }

    public function getCMSFields()
    {

        return new FieldList(
            TextField::create('Title','Title'),
            TextField::create('DestinationURL',"Destination URL"),
            CheckboxField::create('OpenNewWindow','Open in new window')
        );
    }
}