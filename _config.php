<?php

/**
 * Fetches the name of the current module folder name.
 *
 * @return string
**/
define('L49_CUSTOM_MENU_DIR', ltrim(Director::makeRelative(realpath(__DIR__)), DIRECTORY_SEPARATOR));

Object::add_extension('SiteConfig', 'CustomSiteConfig');